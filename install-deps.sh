#!/bin/bash

#Written for RebornOS. All code and artwork was done by Caden Mitchell (TriVoxel.) 
#https://TriVoxel.weebly.com | https://rebornos.org | https://KodeByter.page.link/git

# Stop program if it doesn't have root privileges
if [ "$EUID" -ne 0 ];then
	echo "Installer must be run as root! Try using 'sudo install-deps.sh' instead."
	killall zenity
	zenity --error --text="Installer must be run as root! Try using 'sudo install-deps.sh' instead." --ellipsize --title="Kodebyter Install Dependencies: Error"
	exit
fi

if [[ $1 = -h || $1 = --help ]];then
	echo "Usage: 'install-deps.sh <option>'"
	echo "Options:"
	echo "	-h | --help: Shows this help dialogue"
	echo "	arch: installs dependencies for RebornOS and Arch"
	echo "	debian: installs dependencues for Debian, Ubuntu, and Mint"
	echo "	redhat: installs dependencies for RedHat and Fedora (Not implemented yet.)"
	echo "	suse: installs dependencies for OpenSUSE"
	exit
fi

platform=$1
if [ $platform = arch ];then
	sudo pacman -S --no-confirm zenity python3 git
	echo "Dependencies installed!"
	killall zenity
	zenity --info --text="Arch dependencies installed! You can now install KodeByter!" --ellipsize --title="KodeByter Arch Deps were Installed!" --window-icon=./kodebyter.svg
	exit
elif [ $platform = debian ];then
	sudo apt install -y zenity python3 git
	echo "Dependencies installed!"
	killall zenity
	zenity --info --text="Debian dependencies installed! You can now install KodeByter!" --ellipsize --title="KodeByter Debian Deps were Installed!" --window-icon=./kodebyter.svg
	exit
elif [ $platform = suse ];then
	sudo zypper install --non-interactive zenity python3 git
	echo "Dependencies installed!"
	killall zenity
	zenity --info --text="OpenSUSE dependencies installed! You can now install KodeByter!" --ellipsize --title="KodeByter OpenSUSE Deps were Installed!" --window-icon=./kodebyter.svg
	exit
elif [ $platform = redhat ];then
	echo "Feature unimplimented. Sorry about that!"
	echo "Use you package manager to install 'zenity', 'python3', and 'git'."
	exit
else
	echo "Usage: 'install-deps.sh <option>'"
	echo "Use 'install-deps.sh --help' for more options."
	exit
fi