#!/bin/bash

#Written for Reborn OS. All code and artwork was done by Caden Mitchell (TriVoxel.) 
#https://TriVoxel.weebly.com | https://rebornos.org | https://KodeByter.page.link/git

rm -rvf /tmp/kodebyter-installer/
git clone -b linux-installer --single-branch https://gitlab.com/TriVoxel/kodebyter.git /tmp/kodebyter-installer

export SUDO_ASKPASS="/tmp/kodebyter-installer/askpass.sh"

install='/usr/share/kodebyter/'

(( EUID != 0 )) && exec sudo -A -- "$0" "$@"

if [ "$EUID" -ne 0 ]
	then killall zenity
	echo "Installer must be run as root."
	zenity --error --text="kodebyter installer failed. Insufficient privileges." --ellipsize --title="kodebyter Installer: Error" --window-icon=/tmp/kodebyter/kodebyter.svg
	exit
else
	killall zenity
	if [ ! -d /tmp/kodebyter/ ];then
		zenity --info --text="KodeByter installer is currently fetching files. This will close when the operation is completed." --ellipsize --title="kodebyter Installer" --window-icon=./kodebyter.svg & echo "Removing old installer files..." ; fg
		rm -rvf /tmp/kodebyter/
		echo "Downloading program files..."
		git clone -b master --single-branch https://gitlab.com/TriVoxel/kodebyter.git /tmp/kodebyter/
		chmod +x /tmp/kodebyter/askpass.sh
		echo "Download complete."
		killall zenity
	fi
	zenity --info --text="kodebyter is currently installing. This will close when the operation is completed." --ellipsize --title="kodebyter Installer" --window-icon=./kodebyter.svg & echo "Beginning installation..." ; fg
	echo "--------------------------------"
	echo "++++++++++++++++++++++++++++++++"
	echo "--------------------------------"
	echo "Creating directories..."
	mkdir -v $install
	echo "Installing program files..."
	cp -v /tmp/kodebyter/kodebyter /bin/
	cp -v /tmp/kodebyter/kodebyter.desktop /usr/share/applications/
	cp -v /tmp/kodebyter/kodebyter.svg /usr/share/icons/
	echo "Installing updater..."
	cp -v /tmp/kodebyter/kodebyter-updater /bin/
	cp -v /tmp/kodebyter/kodebyter-updater.desktop /usr/share/applications/
	cp -v /tmp/kodebyter/kodebyter-updater.svg /usr/share/icons/
	cp -v /tmp/kodebyter/askpass.sh $install
	echo "Changing permissions of program files..."
	chmod -v +x /bin/kodebyter
	chmod -v +x /bin/kodebyter-updater
	chmod -v +x /usr/share/applications/kodebyter.desktop
	chmod -v +x /usr/share/applicatios/kodebyter-updater.desktop
	chmod -v +x /usr/share/kodebyter/askpass.sh
	echo "Linking scripts..."
	ln -sv /bin/kodebyter /usr/bin/
	ln -sv /bin/kodebyter-updater /usr/bin/kodebyter-updater/
	echo "Cleaning up..."
	rm -rvf /tmp/kodebyter
	rm -rvf /tmp/kodebyter-installer
	echo "Done."
	killall zenity
	zenity --info --text="The kodebyter installer has finished. You may exit the program." --ellipsize --title="KodeByter was Installed" --window-icon=./kodebyter.svg
	echo "Updater completed."
	echo "Installer completed."
	echo "Run the command 'kodebyter' or 'python3 /bin/kodebyter' to run KodeByter."
	echo "To update the program, run the command 'kodebyter-updater' or 'sh /bin/kodebyter-updater' and it will update automatically."
	exit
fi