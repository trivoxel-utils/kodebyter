# KodeByter

A simple Caesar cipher CLI program to encrypt and decrypt messages. Based on the Python coding backend.

## Features:
* Small file size.
* Automated installer and updater
* Fast loading time using your default terminal.
* Runs on any platform (32 bit, 64 bit, ARMhf.)
* Coded in Python for speed and stability.
* Can be used along with email, social media, and direct messaging apps for easy sharing.
* Is completely free and open source. Feel free to modify this source and make it your own.

## Installation:
Simply run the fully automated `install-deps.sh` dependency installer with one of the following options for your platform:
* `arch`
* `debian`
* `redhat` (WIP)
* `suse`
Then, run the `installer.sh` script to automatically fetch the most current files from online and install it to your system automatically regardless of distribution! Done! :)

_Get it today on [Reborn OS](https://rebornos.org) repositories!_
