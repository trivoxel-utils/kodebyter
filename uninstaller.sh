#!/bin/bash

#Written for RebornOS. All code and artwork was done by Caden Mitchell (TriVoxel.) 
#https://TriVoxel.weebly.com | https://rebornos.org | https://kodebyter.page.link/git

killall zenity

export SUDO_ASKPASS="/usr/share/kodebyter/askpass.sh"

install='/usr/share/kodebyter'

(( EUID != 0 )) && exec sudo -A -- "$0" "$@"

if [ "$EUID" -ne 0 ]
	then echo "Updater must be run as root."
		zenity --error --text="KodeByter uninstaller failed. Insufficient privileges." --ellipsize --title="KodeByter Uninstaller" --window-icon=/tmp/kodebyter/kodebyter.png
	exit
else
	zenity --info --text="KodeByter is currently uninstalling. This will close when the operation is completed." --ellipsize --title="KodeByter Uninstaller" --window-icon=/usr/share/kodebyter/kodebyter.png & echo "Beginning purge..." ; fg
	echo "--------------------------------"
	echo "++++++++++++++++++++++++++++++++"
	echo "--------------------------------"
	echo "Purging old updater files..."
	rm -rvf /tmp/kodebyter/
	echo "Purging all KodeByter files..."
	rm -rv $install
	rm -v /bin/kodebyter
	rm -v /usr/bin/kodebyter
	rm -v /usr/share/applications/kodebyter.desktop
	rm -v /usr/share/applications/kodebyter-updater.desktop
	rm -v /usr/share/icons/kodebyter.svg
	rm -v /usr/share/icons/kodebyter-updater.svg
	rm -v /usr/bin/kodebyter-updater
	rm -v /bin/kodebyter-updater
	killall zenity
	zenity --info --text="The KodeByter uninstaller has finished. No more program files present." --ellipsize --title="KodeByter was Purged" --window-icon=/usr/share/kodebyter/kodebyter.png
	echo "Uninstallation completed."
fi